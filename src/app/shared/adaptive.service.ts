import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, fromEvent } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AdaptiveService implements OnDestroy {
  public windowWidth$ = new BehaviorSubject(window.innerWidth);

  private windowEvent$ = fromEvent(window, 'resize').subscribe(() =>
    this.windowWidth$.next(window.innerWidth)
  );

  ngOnDestroy(): void {
    this.windowEvent$.unsubscribe();
  }
}
