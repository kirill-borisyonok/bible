export interface NavigationItem {
  tabName: string;
  path: string;
}

export interface DailyWordResponse {
  dailyWordId: string;
  word: string;
  description: string;
  wordDate: string;
}

export interface DescribeResponse {
  wordDescriptionId: string;
  input: string;
  description: string;
  addedDescription: string;
}

export interface ContinueResponse {
  wordDescriptionId: string;
  input: string;
  description: string;
  addedDescription: string;
}
