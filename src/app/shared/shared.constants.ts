import { Navigation } from './shared.enums';
import {NavigationItem} from './shared.interfaces';

export const NAVIGATION: NavigationItem[] = [
  {
    tabName: 'Главная',
    path: Navigation.Home,
  },
  {
    tabName: 'О проекте',
    path: Navigation.About,
  },
];
