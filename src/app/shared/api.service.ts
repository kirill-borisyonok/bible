import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable } from 'rxjs';
import {
  ContinueResponse,
  DailyWordResponse,
  DescribeResponse,
} from './shared.interfaces';

@Injectable({ providedIn: 'root' })
export class ApiService {
  private readonly http = inject(HttpClient);

  private readonly api = 'http://77.243.80.89:5000/api/';

  public getDaileWord(): Observable<DailyWordResponse> {
    return this.http.get(
      `${this.api}word/daily`
    ) as Observable<DailyWordResponse>;
  }

  public getDescribe(input: string): Observable<DescribeResponse> {
    return this.http.post(`${this.api}phrase/describe`, {
      input,
    }) as Observable<DescribeResponse>;
  }

  public getLesson(input: string): Observable<DescribeResponse> {
    return this.http.post(`${this.api}phrase/lesson`, {
      input,
    }) as Observable<DescribeResponse>;
  }

  public getContinue(WordDescriptionId: string): Observable<ContinueResponse> {
    return this.http.post(`${this.api}phrase/continue`, {
      WordDescriptionId,
    }) as Observable<DescribeResponse>;
  }
}
