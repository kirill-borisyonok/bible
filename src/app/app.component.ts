import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { RouterModule } from '@angular/router';
import { ApiService } from './shared/api.service';
import { LoaderComponent } from './shared/components/loader/loader.component';
import { NAVIGATION } from './shared/shared.constants';
import { Navigation } from './shared/shared.enums';
import { DailyWordResponse } from './shared/shared.interfaces';

@Component({
  standalone: true,
  imports: [RouterModule, CommonModule, LoaderComponent],
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  private readonly api = inject(ApiService);

  protected readonly navigation = NAVIGATION;
  protected readonly navigationPaths = Navigation;
  protected readonly wordTitle = 'Слово на сегодня:';

  protected dailyData!: DailyWordResponse;

  constructor() {
    this.api
      .getDaileWord()
      .pipe(takeUntilDestroyed())
      .subscribe((data) => (this.dailyData = data));
  }
}
