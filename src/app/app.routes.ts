import {Route} from '@angular/router';
import AboutComponent from './pages/about/about.component';
import HomeComponent from './pages/home/home.component';
import {Navigation} from './shared/shared.enums';

export const appRoutes: Route[] = [
  {
    path: Navigation.Home,
    loadComponent: () => HomeComponent,
  },
  {
    path: Navigation.About,
    loadComponent: () => AboutComponent,
  }
];
