import { ClipboardModule } from '@angular/cdk/clipboard';
import { CommonModule } from '@angular/common';
import { Component, DestroyRef, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormBuilder, FormControl, ReactiveFormsModule } from '@angular/forms';
import {
  NgxExtendedPdfViewerModule,
  pdfDefaultOptions,
} from 'ngx-extended-pdf-viewer';
import { ApiService } from 'src/app/shared/api.service';
import { LoaderComponent } from 'src/app/shared/components/loader/loader.component';
import {
  ContinueResponse,
  DescribeResponse,
} from 'src/app/shared/shared.interfaces';

@Component({
  selector: 'home',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgxExtendedPdfViewerModule,
    ClipboardModule,
    LoaderComponent,
  ],
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export default class HomeComponent {
  private readonly fb = inject(FormBuilder);
  private readonly api = inject(ApiService);
  private readonly destroyRef = inject(DestroyRef);

  protected formControl: FormControl<string>;

  protected readonly placeholder = 'Введите текст';
  protected decribeData!: DescribeResponse | null;
  protected continueData!: ContinueResponse | null;
  protected loadingStatus = false;

  constructor() {
    this.formControl = this.fb.nonNullable.control('');
    pdfDefaultOptions.assetsFolder = 'bleeding-edge';
  }

  pdfSrc = './assets/Bible.pdf';

  protected submitDescribe(): void {
    this.loadingStatus = true;
    this.continueData = null;
    this.decribeData = null;
    this.api
      .getDescribe(this.formControl.value)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((data) => {
        this.decribeData = data;
        this.loadingStatus = false;
      });
  }

  protected submitLesson(): void {
    this.loadingStatus = true;
    this.continueData = null;
    this.decribeData = null;
    this.api
      .getLesson(this.formControl.value)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((data) => {
        this.decribeData = data;
        this.loadingStatus = false;
      });
  }

  protected submitContinue(): void {
    this.loadingStatus = true;
    if (this.decribeData) {
      this.api
        .getContinue(this.decribeData.wordDescriptionId)
        .pipe(takeUntilDestroyed(this.destroyRef))
        .subscribe((data) => {
          this.continueData = data;
          this.loadingStatus = false;
        });
    }
  }
}
